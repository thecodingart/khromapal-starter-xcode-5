//
//  PaletteContainer.swift
//  KhromaPal
//
//  Created by Brandon Levasseur on 10/9/14.
//  Copyright (c) 2014 RayWenderlich. All rights reserved.
//

import Foundation

@objc protocol PaletteDisplayContainer {
    func rwt_currentlyDisplayedPalette() -> ColorPalette?
}
@objc protocol PaletteSelectionContainer {
    func rwt_currentlySelectedPalette() -> ColorPalette?
}