//
//  NavigationController.swift
//  KhromaPal
//
//  Created by Brandon Levasseur on 10/9/14.
//  Copyright (c) 2014 RayWenderlich. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, PaletteDisplayContainer, PaletteSelectionContainer {

    func rwt_currentlyDisplayedPalette() -> ColorPalette? {
        if let tvc = topViewController as? PaletteDisplayContainer {
            return tvc.rwt_currentlyDisplayedPalette()
        }
        
        return nil
    }

    func rwt_currentlySelectedPalette() -> ColorPalette? {
        if let vc = topViewController as? PaletteSelectionContainer {
            return vc.rwt_currentlySelectedPalette()
        }
        return nil
    }
}
