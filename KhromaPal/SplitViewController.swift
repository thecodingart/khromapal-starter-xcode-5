//
//  SplitViewController.swift
//  KhromaPal
//
//  Created by Brandon Levasseur on 10/8/14.
//  Copyright (c) 2014 RayWenderlich. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController!, ontoPrimaryViewController primaryViewController: UIViewController!) -> Bool {
        if let selectionCont = primaryViewController as? PaletteSelectionContainer {
            if let displayCont = secondaryViewController as? PaletteDisplayContainer {
                let selectedPalette = selectionCont.rwt_currentlySelectedPalette()
                let displayedPalette = displayCont.rwt_currentlyDisplayedPalette()
                if selectedPalette? != nil && selectedPalette == displayedPalette {
                    return false
                }
            }
        }
        
        //We don't want anything to happen.
        return true
    }
    
    func splitViewController(splitViewController: UISplitViewController, separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController!) -> UIViewController? {
        if let paletteDisplayCont = primaryViewController as? PaletteDisplayContainer {
            if paletteDisplayCont.rwt_currentlyDisplayedPalette() != nil {
                return nil
            }
        }
        let vc = storyboard?.instantiateViewControllerWithIdentifier("NoPaletteSelected") as UIViewController
        return NavigationController(rootViewController: vc)
    }

}
