//
//  NoPaletteSelectedViewController.swift
//  KhromaPal
//
//  Created by Brandon Levasseur on 10/12/14.
//  Copyright (c) 2014 RayWenderlich. All rights reserved.
//

import UIKit

class NoPaletteSelectedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let svc = splitViewController {
            navigationItem.setLeftBarButtonItem(svc.displayModeButtonItem(), animated: true)
        }
    }
}
